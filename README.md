# student manager windows application
windows version of the student manager application

## integration into scene builder
to work with scene builder in this project you need to configure it.
1. open scene builder 8
2. click the settings symbol to the right of the "Library" label and search box
3. select "JAR/FXML Manager"
4. select "Manually add Library from repository"
5. enter the following values:
    - Group ID: com.jfoenix
    - Artifact ID: jfoenix
    - Version: 8.0.9
6. click "Add JAR"

## dependencies
### log4j2
##### description
"Logging does have its drawbacks. It can slow down an application. If too verbose, it can cause scrolling blindness.
To alleviate these concerns, log4j is designed to be reliable, fast and extensible.
Since logging is rarely the main focus of an application,
the log4j API strives to be simple to understand and to use." - logging.apache.org/log4j/2.x/manual

##### documentation
https://logging.apache.org/log4j/2.x/manual/index.html

##### license
Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0

### jfoenix
##### description
"JFoenix is an open source java library, that implements Google Material Design using java components." - jfoenix.com/documentation

##### purpose
since this app is available for windows and android it is important to have a unified look and feel between the two builds.
the jfoenix library brings the material design that is commonly used for android apps to javafx
and enables us to provide a unified cross-platform experience

##### documentation
http://jfoenix.com/documentation.html

##### license
Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0

### datafx (deprecated)
javafx loader
