/*
 * windows version of the student-manager application
 * Copyright (C) 2019 Koch, Ikari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mybaobab.studentmanager.windows;

import com.jfoenix.assets.JFoenixResources;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.svg.SVGGlyph;
import com.jfoenix.svg.SVGGlyphLoader;
import com.mybaobab.studentmanager.windows.control.MainController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main extends Application {

    public static final Locale LOCALE = Locale.GERMANY;
    public static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("translations", LOCALE);
    public static final ContainerAnimations transitionAnimation = ContainerAnimations.SWIPE_LEFT;

    private static final float WINDOW_TO_SCREEN_RATIO_WIDTH = 0.4f;
    private static final float WINDOW_TO_SCREEN_RATIO_HEIGHT = 0.75f;
    private static final int FALLBACK_WINDOW_WIDTH = 800;
    private static final int FALLBACK_WINDOW_HEIGHT = 600;

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    @FXMLViewFlowContext
    private ViewFlowContext flowContext;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // text anti-aliasing
        System.setProperty("prism.lcdtext", "false");

        new Thread(() -> {
            try {
                SVGGlyphLoader.loadGlyphsFont(Main.class.getResourceAsStream("/fonts/icomoon.svg"), "icomoon.svg");
            } catch (IOException ex) {
                LOGGER.error("could not load icomoon.svg", ex);
            }
        }).start();

        final Flow flow = new Flow(MainController.class);
        flow.getViewConfiguration().setResources(RESOURCE_BUNDLE);
        final DefaultFlowContainer container = new DefaultFlowContainer();
        flowContext = new ViewFlowContext();
        flowContext.register("Stage", stage);
        flow.createHandler(flowContext).start(container);

        final JFXDecorator decorator = new JFXDecorator(stage, container.getView());
        decorator.setCustomMaximize(true);
        decorator.setGraphic(new SVGGlyph(""));

        stage.setTitle(RESOURCE_BUNDLE.getString("main.title"));

        double width = FALLBACK_WINDOW_WIDTH;
        double height = FALLBACK_WINDOW_HEIGHT;
        try {
            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
            width = WINDOW_TO_SCREEN_RATIO_WIDTH * bounds.getWidth();
            height = WINDOW_TO_SCREEN_RATIO_HEIGHT * bounds.getHeight();
        } catch (Exception ex) {
            LOGGER.info("could not get screen size. falling back to default values", ex);
        }

        final Scene scene = new Scene(decorator, width, height);
        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(
            JFoenixResources.load("css/jfoenix-fonts.css").toExternalForm(),
            JFoenixResources.load("css/jfoenix-design.css").toExternalForm(),
            Main.class.getResource("/css/student-manager-windows.css").toExternalForm()
        );
        stage.setScene(scene);
        stage.show();
    }
}
