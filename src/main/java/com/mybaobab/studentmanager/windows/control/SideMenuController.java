/*
 * windows version of the student-manager application
 * Copyright (C) 2019 Koch, Ikari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mybaobab.studentmanager.windows.control;

import com.jfoenix.controls.JFXListView;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import java.util.Objects;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javax.annotation.PostConstruct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ViewController(value = "/fxml/SideMenuView.fxml")
public class SideMenuController {

    private static final Logger LOGGER = LogManager.getLogger(SideMenuController.class);

    @FXMLViewFlowContext
    private ViewFlowContext context;
    @FXML
    @ActionTrigger("textfield")
    private Label textfield;
    @FXML
    private JFXListView<Label> sideList;

    @PostConstruct
    @FXML
    private void init() {
        Objects.requireNonNull(context, "context");
        FlowHandler contentFlowHandler = (FlowHandler) context.getRegisteredObject("ContentFlowHandler");
        sideList.propagateMouseEventsToParent();
        sideList.getSelectionModel().selectedItemProperty().addListener((o, oldVal, newVal)
            -> new Thread(() -> Platform.runLater(() -> {
                if (newVal != null) {
                    try {
                        contentFlowHandler.handle(newVal.getId());
                    } catch (VetoException | FlowException ex) {
                        LOGGER.error("could not load side menu", ex);
                    }
                }
            })
        ).start());

        Flow contentFlow = (Flow) context.getRegisteredObject("ContentFlow");
        bindNodeToController(textfield, LoginController.class, contentFlow);
    }

    private void bindNodeToController(Node node, Class<?> controllerClass, Flow flow) {
        flow.withGlobalLink(node.getId(), controllerClass);
    }
}
