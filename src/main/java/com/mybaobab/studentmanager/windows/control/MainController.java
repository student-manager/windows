/*
 * windows version of the student-manager application
 * Copyright (C) 2019 Koch, Ikari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mybaobab.studentmanager.windows.control;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXPopup.PopupHPosition;
import com.jfoenix.controls.JFXPopup.PopupVPosition;
import com.jfoenix.controls.JFXRippler;
import com.mybaobab.studentmanager.windows.Main;
import com.mybaobab.studentmanager.windows.util.ExtendedAnimatedFlowContainer;
import com.mybaobab.studentmanager.windows.util.Flow;
import io.datafx.controller.ViewConfiguration;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import java.io.IOException;
import javafx.animation.Transition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import javax.annotation.PostConstruct;

@ViewController(value = "/fxml/MainView.fxml")
public final class MainController {

    private static final Duration containerAnimationDuration = Duration.millis(320);

    @FXMLViewFlowContext
    private ViewFlowContext context;
    @FXML
    private StackPane root;
    @FXML
    private StackPane titleBurgerContainer;
    @FXML
    private JFXHamburger titleBurger;
    @FXML
    private StackPane optionsBurger;
    @FXML
    private JFXRippler optionsRippler;
    @FXML
    private JFXDrawer drawer;
    private JFXPopup toolbarPopup;

    private final ViewConfiguration viewConfiguration = new ViewConfiguration();

    @PostConstruct
    @FXML
    private void init() throws IOException, FlowException {
        initMainPopup();
        viewConfiguration.setResources(Main.RESOURCE_BUNDLE);
        initMain();
        initSideMenu();
    }

    private void initMainPopup() throws IOException {
        final FXMLLoader loader = new FXMLLoader(
            getClass().getResource("/fxml/MainPopupView.fxml"),
            Main.RESOURCE_BUNDLE
        );
        loader.setController(new MainPopupController());
        toolbarPopup = new JFXPopup(loader.load());
        optionsBurger.setOnMouseClicked(e ->
            toolbarPopup.show(
                optionsBurger,
                PopupVPosition.TOP,
                PopupHPosition.RIGHT,
                -12,
                15
            ));
    }

    private void initMain() throws FlowException {
        // create the inner flow and content
        context = new ViewFlowContext();
        // set the default controller
        final Flow innerFlow = new Flow(LoginController.class, viewConfiguration);
        final FlowHandler flowHandler = innerFlow.createHandler(context);
        context.register("ContentFlowHandler", flowHandler);
        context.register("ContentFlow", innerFlow);
        drawer.setContent(flowHandler.start(new ExtendedAnimatedFlowContainer(
            containerAnimationDuration,
            Main.transitionAnimation
        )));
        context.register("ContentPane", drawer.getContent().get(0));
    }

    private void initSideMenu() throws FlowException {
        // side controller will add links to the content flow
        final Flow sideMenuFlow = new Flow(SideMenuController.class, viewConfiguration);
        final FlowHandler sideMenuFlowHandler = sideMenuFlow.createHandler(context);
        drawer.setSidePane(sideMenuFlowHandler.start(new ExtendedAnimatedFlowContainer(
            containerAnimationDuration,
            ContainerAnimations.FADE
        )));
        drawer.setOnDrawerOpening(e -> {
            final Transition animation = titleBurger.getAnimation();
            animation.setRate(1);
            animation.play();
        });
        drawer.setOnDrawerClosing(e -> {
            final Transition animation = titleBurger.getAnimation();
            animation.setRate(-1);
            animation.play();
        });
        titleBurgerContainer.setOnMouseClicked(e -> {
            if (drawer.isClosed() || drawer.isClosing()) {
                drawer.open();
            } else {
                drawer.close();
            }
        });
    }
}
