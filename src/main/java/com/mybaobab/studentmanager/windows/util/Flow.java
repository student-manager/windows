/*
 * windows version of the student-manager application
 * Copyright (C) 2019 Koch, Ikari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mybaobab.studentmanager.windows.util;

import io.datafx.controller.ViewConfiguration;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.util.ResourceBundle;

/**
 * This class serves as a fixed version of the {@link io.datafx.controller.flow.Flow} class,
 * which it subclasses for that purpose. Consequently, it should be used in place of the original.
 */
public class Flow extends io.datafx.controller.flow.Flow {
    public Flow(Class<?> startViewControllerClass, ViewConfiguration viewConfiguration) {
        super(startViewControllerClass, viewConfiguration);
    }

    /**
     * <p>creates a handler that can be used to run the flow. the Flow class provides only the definition of a flow.
     * to run a flow in the javafx scene graph a{@link FlowHandler} is needed.</p>
     *
     * <p>this fixed version also takes the ViewConfiguration into account,
     * allowing, among other things, the use of a {@link ResourceBundle}.</p>
     *
     * @param flowContext The context for the flow
     * @return a flow handler to run the flow
     */
    @Override
    public FlowHandler createHandler(ViewFlowContext flowContext) {
        if (getViewConfiguration() != null) {
            return new FlowHandler(this, flowContext, getViewConfiguration());
        } else {
            return new FlowHandler(this, flowContext);
        }
    }
}
